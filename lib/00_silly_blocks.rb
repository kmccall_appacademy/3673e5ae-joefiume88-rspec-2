def reverser(&result)
  res = ''
  result.call.split(' ').each do |word|
    res << word.reverse + ' '
  end
  
  res[0...-1]
end

def adder(num=1, &block)
  block.call + num
end

def repeater(n=1, &block)
  n.times { block.call }
end
